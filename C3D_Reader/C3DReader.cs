﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace C3D_Reader
{
	/// <summary>
	/// An internal Paramater Block. This represents the main paramater block within a C3D file
	/// and has a group ID number, a name, a description and a list of assicatated properties
	/// along with it.
	/// </summary>
	internal class Paramaters
	{
		// Group's ID number, so we can put the correct properties in the correct pramater block.
		public int groupId = -1;
		// Name for the Paramater block.
		public string name = "";
		// Description of the Paramater block, for example what it stores.
		public string description = "";
		// All the properties within the Paramater block.
		public List<Properties> properties = new List<Properties>();
		
		/// <summary>
		/// Constructor
		/// </summary>
		public Paramaters()
		{
			this.groupId = -1;
			this.name = "";
			this.description = "";
			this.properties = new List<Properties>();
		}

		/// <summary>
		/// Reset the internal data for the paramater
		/// </summary>
		public void resetBlock()
		{
			this.groupId = -1;
			this.name = "";
			this.description = "";
			this.properties = new List<Properties>();
		}
	}


	/// <summary>
	/// An internal Property value. This will hold type generic data with a specific name to
	/// identify what it's contents hold.
	/// It can hold floats, for such items as Frame Rate, or strings such as the mesurements
	/// or arrays of strings such as all the marker labels.
	/// </summary>
	internal class Properties
	{
		public string name = "";
		public object value = null;

		public Properties(string name)
		{
			this.name = name;
			this.value = null;
		}
	}

	/// <summary>
	/// Class to represent a simple vector 3 value.
	/// </summary>
	public class Vector3
	{
		private float _x = 0.0f;
		private float _y = 0.0f;
		private float _z = 0.0f;

		/// <summary>
		/// Constructor, Defaults the X, Y and Z values to 0.0
		/// </summary>
		public Vector3()
		{
			this._x = 0.0f;
			this._y = 0.0f;
			this._z = 0.0f;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="newX">The X value to input</param>
		/// <param name="newY">The Y value to input</param>
		/// <param name="newZ">The Z value to input</param>
		public Vector3(float newX, float newY, float newZ)
		{
			this._x = newX;
			this._y = newY;
			this._z = newZ;
		}

		public float x
		{
			//set the x value.
			set { this._x = value; }
			//get the x value 
			get { return this._x; }
		}

		public float y
		{
			//set the y value.
			set { this._y = value; }
			//get the y value 
			get { return this._y; }
		}

		public float z
		{
			//set the z value.
			set { this._z = value; }
			//get the z value 
			get { return this._z; }
		}
	}
/// <summary>
	/// Class to handle and deal with the reading in of C3D files, and allowing
	/// a easy API to access the marker data, and the paramaters with the data
/// </summary>
	public class C3DReader
	{
		// Constant value for the C3D file
		private const int c3dFileKeyValue = 80;

		private int parameterBlock = 0;
		private int markerCount = 0;
		private int analogMesasurements = 0;
		private int firstFrame = 0;
		private int lastFrame = 0;
		private int maxFrameGap = 0;
		private float scaleFactor = 0.0f;
		private int dataStart = 0;
		private int sampleRate = 0;
		private float frameRate = 0.0f;
		private string filePath = "";

		private int numberOfParamaters = 0;
		private int processorType = 0;
		private List<Paramaters> paramaterDict = new List<Paramaters>();

		/// <summary>
		/// Generic Constructor, will reset all the values to their default values
		/// </summary>
		public C3DReader()
		{
			this.reset();
		}

		/// <summary>
		/// Generic Constructor. Will automaticly load data from the given C3D file path
		/// </summary>
		/// <param name="filePath">Path to the C3D file to read from</param>
		public C3DReader(string filePath)
		{
			this.reset();
			this.loadFile(filePath);
		}

		/// <summary>
		/// resets all the inernals values to their defaults
		/// </summary>
		private void reset()
		{
			this.parameterBlock = 0;
			this.markerCount = 0;
			this.analogMesasurements = 0;
			this.firstFrame = 0;
			this.lastFrame = 0;
			this.maxFrameGap = 0;
			this.scaleFactor = 0;
			this.dataStart = 0;
			this.sampleRate = 0;
			this.frameRate = 0.0f;
			this.filePath = "";

			this.numberOfParamaters = 0;
			this.processorType = 0;
			this.paramaterDict = new List<Paramaters>();
		}

		/// <summary>
		///	Load the given c3d file into the object. Will read the headers and
		///	paramater sections of the file, but will not read any of the actual
		///	data, as that is called on a frame basis.
		/// </summary>
		/// <param name="filePath">Path to the C3D file to read from</param>
		public void loadFile(string filePath)
		{
			this.filePath = filePath;
			BinaryReader fileObj = new BinaryReader(File.Open(filePath, FileMode.Open));
			this.readHeader(fileObj);
			this.readParamaters(fileObj);
			fileObj.Close();
		}

		/// <summary>
		/// Internal Method to read the header of a c3d file and populate the 
		/// object with the data
		/// </summary>
		/// <param name="handle">File handle object to the open c3d file</param>
		/// <exception cref="ApplicationException">If the file is not a valid C3D file</exception>
		private void readHeader(BinaryReader handle)
		{
			handle.BaseStream.Seek(0, SeekOrigin.Begin);
			this.parameterBlock = handle.ReadByte();                    // word 1
			if (handle.ReadByte() != C3DReader.c3dFileKeyValue)
			{
				this.raise("Not a valid C3D file");
			}
			this.markerCount = handle.ReadInt16();                      // word 2
			this.analogMesasurements = handle.ReadInt16();              // word 3
			this.firstFrame = handle.ReadInt16();                       // word 4
			this.lastFrame = handle.ReadInt16();                        // word 5
			this.maxFrameGap = handle.ReadInt16();                      // word 6
			this.scaleFactor = handle.ReadSingle();                     // word 7-8
			this.dataStart = handle.ReadInt16();                        // word 9
			this.sampleRate = handle.ReadInt16();                       // word 10
			this.frameRate = handle.ReadSingle();                       // word 11-12
			// More can be added as needed
		}

		/// <summary>
		/// Helper function to read a given amount of bytes and return a string from the 
		/// binararyReader
		/// </summary>
		/// <param name="handle">The BinaryReader object to read from</param>
		/// <param name="length">The lenght of the string to read</param>
		/// <returns>A string from the amount of bytes from the fle</returns>
		private string readStringFromByte(BinaryReader handle, int length)
		{
			byte[] input = handle.ReadBytes(length);
			UTF8Encoding enc = new UTF8Encoding();
			string str = enc.GetString(input);
			return str;
		}

		/// <summary>
		/// Create a new paramater based off the given inputs
		/// </summary>
		/// <param name="handle">The BinaryReader object to read from</param>
		/// <param name="name">The name of the paramater</param>
		/// <param name="groupId">The paramater ID</param>
		/// <returns>The newly created paramater object</returns>
		private Paramaters newParamater(BinaryReader handle, string name, int groupId)
		{
			Paramaters newParama = new Paramaters();
			newParama.groupId = groupId;
			newParama.name = name;
			int paramaDescriptionLenght = handle.ReadByte();
			if (paramaDescriptionLenght != 0)
			{
				newParama.description = this.readStringFromByte(handle, paramaDescriptionLenght);
			}
			return newParama;
		}

		/// <summary>
		/// Read the whole paramater block from the C3D file, creating all the paramaters and
		/// reading in their properties
		/// </summary>
		/// <param name="handle">The BinaryReader object to read from</param>
		private void readParamaters(BinaryReader handle)
		{
			handle.BaseStream.Seek(512, SeekOrigin.Begin);
			handle.ReadByte();
			handle.ReadByte();
			this.numberOfParamaters = handle.ReadByte();
			this.processorType = handle.ReadByte();

			while(true){
				int nameLength = handle.ReadByte();
				int groupId = Math.Abs(handle.ReadSByte());
				string paramaName = readStringFromByte(handle, nameLength);
				int bytesToNextParams = handle.ReadByte();
				if (bytesToNextParams == 0){
					return;
				}
				handle.ReadByte();
				Paramaters paramBlock = this.paramaterDict.Find(param => param.groupId == groupId);
				if (paramBlock == null)
				{
					paramBlock = this.newParamater(handle, paramaName, groupId);
					this.paramaterDict.Add(paramBlock);
					continue;
				}

				Properties newProp = new Properties(paramaName);
				newProp.value = this.readProperty(handle);
				paramBlock.properties.Add(newProp);
			}
		}

		/// <summary>
		/// Read in a property from a C3D file. working out what type of value it is, and storing 
		/// within a property object
		/// </summary>
		/// <param name="handle">The BinaryReader object to read from</param>
		/// <returns>A property object with the read in values</returns>
		private object readProperty(BinaryReader handle)
		{
			int propertyType = handle.ReadSByte();
			int propertyDimensions = handle.ReadByte();
			object valueObj = null;

			if (propertyType == -1)
			{
				// String Value
				int stringSize = handle.ReadByte();
				if (propertyDimensions > 1)
				{
					List<string> tempValue = new List<string>();
					int arraySize = handle.ReadByte();
					for (int idx = 0; idx < arraySize; idx++)
					{
						tempValue.Add(this.readStringFromByte(handle, stringSize).Trim());
					}
					valueObj = tempValue;
				}
				else
				{
					valueObj = this.readStringFromByte(handle, stringSize).Trim();
				}
				handle.ReadByte();
			}
			else if (propertyDimensions != 0)
			{
				List<object> tempValue = new List<object>();
				for (int idx = 0; idx < propertyDimensions + 1; idx++)
				{
					int unUsedIndex = handle.ReadByte();
					if (propertyDimensions > 1)
					{
						unUsedIndex *= handle.ReadByte();
					}

					for (int index = 0; index < unUsedIndex; index++)
					{
						tempValue.Add(this.readPropertyValue(handle, propertyType));
					}

					if (propertyDimensions > 1)
					{
						handle.ReadByte();
						break;
					}
				}
				valueObj = tempValue;

			}
			else
			{
				valueObj = this.readPropertyValue(handle, propertyType);
				handle.ReadByte();
			}
			return valueObj;
		}

		/// <summary>
		/// Helper method to read in the correct data type based off the inputted propertyType
		/// and return it as an Object
		/// </summary>
		/// <param name="handle">The BinaryReader object to read from</param>
		/// <param name="propertyType">an Int of the property 
		/// Type. 1 is a Byte, 
		/// 2 is an Int and 
		/// 4 is a float</param>
		/// <returns>An object of either an Int of a Float</returns>
		private object readPropertyValue(BinaryReader handle, int propertyType)
		{
			object tempObj = null;
			if (propertyType == 1)
			{
				tempObj = handle.ReadByte();
			}
			else if (propertyType == 2)
			{
				tempObj = handle.ReadInt16();
			}
			else if (propertyType == 4)
			{
				tempObj = handle.ReadSingle();
			}
			else
			{
				this.raise("Unkown value given");
			}
			return tempObj;
		}

		/// <summary>
		/// Read in 3 float values that make up the X,Y and Z values for a markers position.
		/// This method expects that the BinaryReader head is in the correct position to read
		/// </summary>
		/// <param name="handle">The BinaryReader object to read from</param>
		/// <returns>A vector object of the X,Y and Z coordinates for that marker</returns>
		private Vector3 readMarkerData(BinaryReader handle)
		{
			Vector3 newVect = new Vector3(
				handle.ReadSingle(),
				handle.ReadSingle(), 
				handle.ReadSingle() 
				);
			return newVect;
		}

		/// <summary>
		/// Helper function to raise if somthing went wrong, with a given message.
		/// </summary>
		/// <param name="errorMessage">The message to error with</param>
		private void raise(string errorMessage)
		{
			System.ApplicationException exception = new System.ApplicationException(errorMessage);
			throw exception;
		}

		/// <summary>
		/// Read a single frame of motion data from the C3D file.
		/// Will return a List of Vector 3 values, one for each marker
		/// </summary>
		/// <param name="frameNumber">The Frame Number wanted.</param>
		/// <returns>A List of Vector3 values that represents the position of each marker for that 
		/// frame</returns>
		public List<Vector3> readFrame(int frameNumber)
		{
			if ((frameNumber < this.firstFrame) || (frameNumber > this.lastFrame))
			{
				this.raise("Invalid Frame Number");
			}
			int startOfDataBlock = (Convert.ToInt16(this.getParamater("POINT", "DATA_START")) - 1) * 512;
			int inspectFrame = frameNumber - this.firstFrame;
			int byteFrame = (this.markerCount * 16) * inspectFrame;

			BinaryReader fileObj = new BinaryReader(File.Open(this.filePath, FileMode.Open));
			fileObj.BaseStream.Seek(startOfDataBlock + byteFrame, SeekOrigin.Begin);
			List<Vector3> resArray = new List<Vector3>();
			for (int marker = 0; marker < this.markerCount; marker++)
			{
				resArray.Add(this.readMarkerData(fileObj));
			}
			fileObj.Close();
			return resArray;
		}

		/// <summary>
		/// Iter Frames is the preferd method for getting all the frames in the particular frame range
		/// This will get all frames from the given start frame to the last stored frame, and intervalies
		/// of 1 frame
		/// </summary>
		/// <param name="start">Frame to start</param>
		/// <returns>A list of list which have Vector3 values in them that make up each frame of
		/// animation</returns>
		public List<List<Vector3>> iterFrame(int start)
		{
			int end = this.lastFrame;
			int iterJump = 1;
			return this.iterFrame(start, end, iterJump);
		}

		/// <summary>
		/// Iter Frames is the preferd method for getting all the frames in the particular frame range
		/// This will get all frames from the given start frame until the requested end frame, and 
		/// intervalies of 1 frame
		/// </summary>
		/// <param name="start">Frame to start</param>
		/// <param name="end">Frame to End</param>
		/// <returns>A list of list which have Vector3 values in them that make up each frame of
		/// animation</returns>
		public List<List<Vector3>> iterFrame(int start, int end)
		{
			int iterJump = 1;
			return this.iterFrame(start, end, iterJump);
		}

		/// <summary>
		/// Iter Frames is the preferd method for getting all the frames in the particular frame range
		/// This will get all frames from the file start frame until the file end frame, and at 
		/// intervalies of 1 frame.
		/// </summary>
		/// <returns>A list of list which have Vector3 values in them that make up each frame of
		/// animation</returns>
		public List<List<Vector3>> iterFrame()
		{
			int start = this.firstFrame;
			int end = this.lastFrame;
			int iterJump = 1;
			return this.iterFrame(start, end, iterJump);
		}

		/// <summary>
		/// Internal Method
		/// Iter Frames is the preferd method for getting all the frames in the particular frame range
		/// This will get all frames from the given start frame until the requested end frame, and 
		/// at intervalies of the given number
		/// </summary>
		/// <param name="start">Frame to start</param>
		/// <param name="end">Frame to stop</param>
		/// <param name="iterJump">Frame to skip each time</param>
		/// <returns>A list of list which have Vector3 values in them that make up each frame of
		/// animation</returns>
		public List<List<Vector3>> iterFrame(int start, int end, int iterJump)
		{
			List<List<Vector3>> returnArray = new List<List<Vector3>>();
			for (int idx = start; idx < end; idx += iterJump)
			{
				returnArray.Add(this.readFrame(idx));
			}
			return returnArray;
		}

		/// <summary>
		/// Gets the amount of markers in the currently loaded C3D file
		/// </summary>
		/// <returns>The number of markers</returns>
		public int getMarkerCount()
		{
			return this.markerCount;
		}

		/// <summary>
		/// Get the list of marker names in the C3D file
		/// </summary>
		/// <returns>A list of strings, with each of the marker names</returns>
		public List<string> getMarkers()
		{
			if (this.markerCount == 0)
			{
				return new List<string>();
			}
			return (List<string>)this.getParamater("POINT", "LABELS");
		}

		/// <summary>
		/// Get the animation start frame
		/// </summary>
		/// <returns>The start frame as an Int</returns>
		public int getFrameStart()
		{
			return this.firstFrame;
		}

		/// <summary>
		/// Get the animation end frame
		/// </summary>
		/// <returns>The end frame as an Int</returns>
		public int getFrameEnd()
		{
			return this.lastFrame;
		}

		/// <summary>
		/// Get the frame rate for the current C3D file
		/// </summary>
		/// <returns>The frame rate as a float</returns>
		public float getFrameRate()
		{
			return this.frameRate;
		}

		/// <summary>
		/// Get the sample rate for the current C3D file
		/// </summary>
		/// <returns>The sample rate as a float</returns>
		public float getSampleRate()
		{
			return this.sampleRate;
		}

		/// <summary>
		/// Gets the path to the currently loaded C3D file
		/// </summary>
		/// <returns>A string path to the currently read C3D file</returns>
		public string getLoadedFilePath()
		{
			return this.filePath;
		}

		/// <summary>
		/// Get a list of all the paramaters within the paramater block of the C3D file
		/// </summary>
		/// <returns>A list of strings, that make up the different paramater blocks</returns>
		public List<string> listParamaters()
		{
			List<string> retArray = new List<string>();
			for (int idx = 0; idx < this.paramaterDict.Count(); idx++)
			{
				retArray.Add(this.paramaterDict[idx].name);
			}
			return retArray;
		}

		/// <summary>
		/// Get a list of all the properties within the given paramater
		/// </summary>
		/// <param name="paramater">The name of the paramater to get the property from</param>
		/// <returns>A list of strings, that make up all the different properties within the
		/// given paramater block</returns>
		public List<string> listParamaterProperties(string paramater)
		{
			Paramaters paramBlock = this.paramaterDict.Find(param => param.name == paramater);
			if (paramBlock == null)
			{
				this.raise("Paramater not found in file");
				return null;
			}

			List<string> retArray = new List<string>();
			for (int idx = 0; idx < paramBlock.properties.Count(); idx++)
			{
				retArray.Add(paramBlock.properties[idx].name);
			}
			return retArray;
		}

		/// <summary>
		/// Return the value of a given property within a given paramater block
		/// </summary>
		/// <param name="paramater">The name of the paramter block</param>
		/// <param name="pramProperty">The name of the property to get</param>
		/// <returns>An object of the properties value</returns>
		public object getParamater(string paramater, string pramProperty)
		{
			Paramaters paramBlock = this.paramaterDict.Find(param => param.name == paramater);
			if (paramBlock == null)
			{
				this.raise("Paramater not found in file");
				return null;
			}
			Properties propertiesBlock = paramBlock.properties.Find(prop => prop.name == pramProperty);
			if (propertiesBlock == null)
			{
				this.raise("Proprity not found in paramater");
				return null;
			}
			return propertiesBlock.value;
		}
	}
}
